const Product = require('../models/Product');
// const auth = require('../auth');
const User = require('../models/User');



    

//s43 Create Product Admin Only

module.exports.addProduct = (reqBody, userData) => {

return User.findById(userData.userId)
        .then((result)=>{
    if(!userData.isAdmin){
        return 'You are not the Admin'
    }else{
    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        category: reqBody.category,
        price: reqBody.price
    });
       
    return newProduct.save().then((product, error)=> {
       
        if(error){
            return false
        }
        
        else{
            return true
        }
    })
}
 })
};


//s43 Get All Active Products
module.exports.getAllActive = () => {
    return Product.find({isActive : true})
     
    
};

//s44 Retrieve Single Product
module.exports.getProduct = (reqParams) =>{
    return Product.findById(reqParams.productId)
    .then(result =>{
        return result
    })
};

//Update Product Controller S44

module.exports.updateProduct = async (reqParams, reqBody, userData) =>{
   
    if(userData.isAdmin === true) {
        let updatedProduct = {
            name: reqBody.name,
            description: reqBody.description,
            category: reqBody.category,
            price: reqBody.price

        }
        //Syntax: findByIdAndUpdate(document ID, updatesToBeApplied)
        return await Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
        .then((updatedProduct, error)=>{
            if(error){
                return false
            } else{
                return true
            }
        })
    } else {
       return await "You are not an Admin"
    }
};

//Archive Product S44
module.exports.archiveProduct = async (userData, reqBody) =>{
    if(userData.isAdmin === true) {

        let archivedProduct = {
           isActive: reqBody.isActive

        }
        //Syntax: findByIdAndUpdate(document ID, updatesToBeApplied)
        return await Product.findByIdAndUpdate(userData.productId,
            archivedProduct).then((archivedProduct, error)=>{
            if(error){
                return false
            } else{
                return true
            }
        })
    } else {
        return await 'You are not an Admin'
    }
}



