const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth')


//Creating Routes



//register route - create user in db
router.post ("/register", (req,res) => {
    userController.registerUser(req.body)
    .then(resultFromController => 
        res.send(resultFromController))
});

//auth
router.post("/login", (req, res)=>{
    userController.loginRegUser(req.body)
    .then(resultFromController => 
        res.send(resultFromController))
});


//s45 Retrieve User Details
router.get("/details", auth.verify, (req, res)=>{
    
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    userController.getUserDetails({id: userData.id})
    .then(resultFromController => 
        res.send(resultFromController))
});

//Stretch Goal: Set user to Admin 
router.put("/setAdmin", auth.verify, (req, res)=>{
    
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    userController.setUserToAdmin(req.body, userData)
    .then(resultFromController => 
        res.send(resultFromController))
});







module.exports = router;
