const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');
// const userController = require('../controllers/userController');


//s43 Create Product (Admin Only)
router.post('/', auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    productController.addProduct(req.body, {userId: userData.id, isAdmin: userData.isAdmin})
    .then(resultFromController => 
        res.send(resultFromController))
});



//s43 Get All Active Products

router.get('/', (req,res)=>{
    productController.getAllActive()
    .then(resultFromController => 
        res.send(resultFromController))
});

//s44 Get Specific Product
router.get("/:productId", (req,res)=>{
    console.log(req.params.productId);
    productController.getProduct(req.params)
    .then(resultFromController => 
        res.send(resultFromController))
})

//SCREENSHOT OF POSTMAN IN RESULTSS FOLDER S43
 
//Update Product Route S44
router.put('/:productId', auth.verify, (req,res)=>{
    //auth
    const isAdminData = auth.decode(req.headers.authorization)
    console.log(isAdminData)

    productController.updateProduct(req.params, req.body, isAdminData)
    .then(resultFromController => 
        res.send(resultFromController))
});

//Archive Product S44 (Admin Only)


router.put('/archive/:productId', auth.verify, (req,res)=>{
    //auth
    const isAdminData = {
        productId: req.params.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    productController.archiveProduct(isAdminData, req.body)
    .then(resultFromController => 
        res.send(resultFromController))
});



//s45 Non-admin User checkout (Create Order)



module.exports = router