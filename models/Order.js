const mongoose = require ('mongoose');

//creating schema

const orderSchema = new mongoose.Schema({
        userId: {
            type: String,
            required: [true, "UserId is required"]
        },
    
            productId: {
                type: String,
                required: [true, "ProductId is required"]
            },
            productPrice: {
                type: Number,
                required: [true, "Price is required"]
            },
            quantity: {
                type: Number,
               default: 0
            },
            totalAmount: {
                type: Number,
                default: 0
            },
            purchasedOn: {
                type: Date,
                default: new Date(),
                immutable: true
            }
        


});






module.exports = mongoose.model("Order", orderSchema)