const mongoose = require ('mongoose');

//creating schema

const productSchema = new mongoose.Schema({
        name: {
            type: String,
            required: [true, "Product Name is required"]
        },
        description: {
            type: String,
            required: [true, "Description is required"]
        },
        category:{
            type: String,
            required: [true, "Category is required"]
        },
        isActive: {
            type: Boolean,
            default: true
        },
        price: {
            type: Number,
            required: [true, "Price is required"]
        },
        createdOn: {
            type: Date,
            default: new Date(),
            immutable: true
        }
       

});

//add quantity in ordered user. for Stretch Goal.


module.exports = mongoose.model("Product", productSchema)