const mongoose = require ('mongoose');

//Creating of Schema

const userSchema = new mongoose.Schema({
      email: {
        type: String,
        required: [true, "Email is Required"]
    },
    password: {
        type: String,
        required: [true, "Password is Required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
   
});
module.exports = mongoose.model("User", userSchema)
